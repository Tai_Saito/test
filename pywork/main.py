import logging
import multiprocessing
import os

import cenv
import db_connector
import flask_base
import preprocess.installs
import preprocess.events
import requests.make_report
import tables.campaigns
import tables.events
import tables.purchases
import tables.users
import utils.logger
# from preprocess.common import CommonColumns


# user_to_platform = {}  # строгая связь 1-к-1
# campaign_to_media = {}  # строгая связь 1-к-1
# user_to_install_time = {}  = нет связи 1-к-1, один пользователь может устанавливать несколько раз
# campaign_to_platform = {}   = нет связи
# user_to_time = {}  = есть дубликаты, и есть ПОЧТИ дубликаты (разница только в revenue)
#
#
# def count_rows(row: pd.Series):
#     user_to_platform.setdefault(row[CommonColumns.appsflyer_id.name], set())
#     user_to_platform[row[CommonColumns.appsflyer_id.name]].add(row[CommonColumns.platform.name])
#     campaign_to_media.setdefault(row[CommonColumns.campaign.name], set())
#     campaign_to_media[row[CommonColumns.campaign.name]].add(row[CommonColumns.media_source.name])
#
#
# df = pd.read_csv(os.path.join('data', 'installs1.csv'), index_col=0, header=0, parse_dates=[1, 2], na_filter=False)
# df = df.drop_duplicates()
# # df['valid'] = df.apply(preprocess.installs.check_row_validity, axis=1)
# # df = df[df['valid'] == True]
# df.apply(count_rows, axis=1)
# # wrong_things = list((x, list(z for z in y.keys() if y[z] > 1)) for x, y in user_to_time.items() if any(z > 1 for z in y.values()))
# # print(wrong_things)
# # assert len(wrong_things) == 0
#
# df = pd.read_csv(os.path.join('data', 'events.csv'), index_col=0, header=0, parse_dates=[1, 2], na_filter=False)
# df = df.drop_duplicates()
# # df['valid'] = df.apply(preprocess.events.check_row_validity, axis=1)
# # df = df[df['valid'] == True]
# df.apply(count_rows, axis=1)
# # wrong_things = list((x, list(z for z in y.keys() if y[z] > 1)) for x, y in user_to_time.items() if any(z > 1 for z in y.values()))
# # print(wrong_things)
# # assert len(wrong_things) == 0
# exit()


def recreate_data():
    with db_connector.conn.get_connection() as session:
        with session.begin():
            query = f"TRUNCATE {tables.campaigns.Campaign.__tablename__} RESTART IDENTITY CASCADE; " \
                    f"TRUNCATE {tables.events.Event.__tablename__} RESTART IDENTITY CASCADE;" \
                    f"TRUNCATE {tables.purchases.Purchase.__tablename__} RESTART IDENTITY CASCADE;" \
                    f"TRUNCATE {tables.users.User.__tablename__} RESTART IDENTITY CASCADE;"
            session.execute(query)
    installs = preprocess.installs.read_file(os.path.join('data', 'installs1.csv'))
    events = preprocess.events.read_file(os.path.join('data', 'events.csv'))
    tables.users.insert_from_dataframes(installs, events)
    tables.campaigns.insert_from_dataframes(installs, events)
    tables.events.insert_from_dataframes(installs, events)
    tables.purchases.insert_from_dataframe(events)


utils.logger.setup_logger('root', logging.DEBUG, filename='root', launch_rotating=True)
db_connector.conn = db_connector.DBConnector(is_main_process=True)
recreate_data()
flask_base.app.run(host='0.0.0.0', port=cenv.FLASK_PORT)
