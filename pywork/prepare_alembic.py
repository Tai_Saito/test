import os

import envsubst


with open('alembic_unbuilt.ini', 'r', encoding='utf8') as f:
    with open('alembic.ini', 'w', encoding='utf8') as f2:
        os.environ[f"PGSQL_LOCAL_HOST"] = os.getenv(f"PGSQL_DOCKER_HOST")
        os.environ[f"PGSQL_LOCAL_PORT_INT"] = os.getenv(f"PGSQL_DOCKER_PORT_INT")
        f2.write(envsubst.envsubst(f.read()))
