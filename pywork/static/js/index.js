function make_report() {
    let error = false;
    const data = {};
    for (let input of $("#search-container input")) {
        input = $(input);
        const val = input.val();
        if (val.length == 0) {
            if (input.prop('required')) {
                error = true;
                input.parent().find('.search-span_error-message').text('обязательное поле');
                input.parent().find('.search-span_error-message').css('visibility', 'visible');
                $('#search-span_error-message_main').css('visibility', 'hidden');
            }
        }
        else {
            input.parent().find('.search-span_error-message').css('visibility', 'hidden');
            data[input.attr('name')] = val;
        }
    }
    if (error) return;
    console.log(data);
    $.getJSON(URL_MAKE_REPORT, data, function(json) {
        console.log(json);
        if (!(json.hasOwnProperty('error') && json['error'] === false)) {
            $('#search-span_error-message_main').text(json['message']);
            $('#search-span_error-message_main').css('visibility', 'visible');
            return;
        }
        $('#search-span_error-message_main').css('visibility', 'hidden');
        $('#data-table tr[id!="data-table_head"]').remove();
        const table = $('#data-table');
        for (const row of json['data']) {
            const tr = $('<tr>');
            tr.append($('<td>', {
                'text': row['campaign']
            }));
            tr.append($('<td>', {
                'text': row['installs']
            }));
            tr.append($('<td>', {
                'text': row['revenue']
            }));
            tr.append($('<td>', {
                'text': row['revenue_usd']
            }));
            table.append(tr);
        }
    });
}