from pandas import Series

import utils.logger
from tables.campaigns import Campaign
from tables.events import Event
from tables.users import User
from utils.types import CommonColumns


def construct_row_insert_data(row: Series):
    logger = utils.logger.get_logger()
    try:
        val = row[CommonColumns.appsflyer_id.name]
        assert val.count('-') == 1
        num1, num2 = map(lambda x: int(x), val.split('-'))
    except (AssertionError, TypeError, ValueError) as exc:
        logger.error(f"row {row}: {CommonColumns.appsflyer_id.name} is not valid", exc_info=exc)
        return
    if not (len(row[CommonColumns.media_source.name]) > 0):
        logger.error(f"row {row}: {CommonColumns.media_source.name} is not valid")
        return
    if row[CommonColumns.media_source.name] == 'restricted':
        if not (len(row[CommonColumns.campaign.name]) == 0):
            logger.error(f"row {row}: {CommonColumns.campaign.name} is not valid")
            return
    else:
        if not (len(row[CommonColumns.campaign.name]) > 0):
            logger.error(f"row {row}: {CommonColumns.campaign.name} is not valid")
            return
    if not (len(row[CommonColumns.platform.name]) > 0):
        logger.error(f"row {row}: {CommonColumns.platform.name} is not valid")
        return
    user = {
        User.appsflyer_id.name: row[CommonColumns.appsflyer_id.name],
        User.platform.name: row[CommonColumns.platform.name]
    }
    campaign = {
        Campaign.name.name: row[CommonColumns.campaign.name],
        Campaign.media_source.name: row[CommonColumns.media_source.name]
    }
    event = {
        Event.user_id.name: row[CommonColumns.appsflyer_id.name],
        Event.campaign_name.name: row[CommonColumns.campaign.name]
    }
    return user, campaign, event
