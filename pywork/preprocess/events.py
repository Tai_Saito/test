import time

import numpy as np
import pandas as pd
from pandas import DataFrame, Series, Timestamp

import preprocess.common
import tables.events
import utils.logger
from tables.events import Event
from tables.purchases import Purchase
from utils.types import CommonColumns


def construct_row_insert_data(row: Series):
    logger = utils.logger.get_logger()
    install_time = row[CommonColumns.install_time.name]
    event_time = row[CommonColumns.event_time.name]
    if not isinstance(install_time, Timestamp):
        logger.error(f"row {row}: {CommonColumns.install_time.name} is not valid")
        return
    if not isinstance(event_time, Timestamp):
        logger.error(f"row {row}: {CommonColumns.event_time.name} is not valid")
        return
    event_time = event_time.to_pydatetime()
    result = preprocess.common.construct_row_insert_data(row)
    if result is None:
        return
    if not (row[CommonColumns.event_name.name] == 'af_purchase'):
        logger.error(f"row {row}: {CommonColumns.event_name.name} is not valid")
        return
    event_revenue = row['event_revenue']
    event_revenue_usd = row['event_revenue_usd']
    if not isinstance(event_revenue, np.float64):
        try:
            event_revenue = float(event_revenue)
        except (TypeError, ValueError):
            logger.error(f"row {row}: {CommonColumns.event_revenue.name} is not valid")
            return
    if not isinstance(event_revenue_usd, np.float64):
        try:
            event_revenue_usd = float(event_revenue_usd)
        except (TypeError, ValueError):
            logger.error(f"row {row}: {CommonColumns.event_revenue_usd.name} is not valid")
            return
    user, campaign, event = result
    event[Event.event_time.name] = event_time
    event[Event.event_name.name] = row[CommonColumns.event_name.name]
    purchase = {
        Purchase.event_revenue.name: event_revenue,
        Purchase.event_revenue_usd.name: event_revenue_usd
    }
    return user, campaign, event, purchase


# def check_row_validity(row: Series):
#     return construct_row_insert_data(row) is not None


def read_file(file_path: str):
    try:
        df = pd.read_csv(file_path, header=0, index_col=0, parse_dates=[1, 2], na_filter=False)
    except (AssertionError, TypeError, ValueError) as exc:
        utils.logger.get_logger().error(f"error in processing {file_path}", exc_info=exc)
        return

    t = time.time()
    print('validating events data...')
    _data: DataFrame = df.apply(construct_row_insert_data, axis=1, result_type='expand')
    _data.columns = ['user_obj', 'campaign_obj', 'event_obj', 'purchase_obj']
    df = df.join(_data)
    _cond = ((pd.notna(df['user_obj'])) & (pd.notna(df['campaign_obj'])) & (pd.notna(df['event_obj'])) & (pd.notna(df['purchase_obj'])))
    df = df[_cond]
    print(f"validated in {time.time() - t} seconds")

    return df
