import time

import pandas as pd
import sqlalchemy.dialects.postgresql as psql
from pandas import DataFrame, Series, Timestamp

import db_connector
import preprocess.common
import utils.logger
from tables.events import Event
from tables.users import User
from utils.types import CommonColumns


def construct_row_insert_data(row: Series):
    logger = utils.logger.get_logger()
    install_time = row[CommonColumns.install_time.name]
    event_time = row[CommonColumns.event_time.name]
    install_time_dt = install_time if isinstance(install_time, Timestamp) else None
    event_time_dt = event_time if isinstance(event_time, Timestamp) else None
    dt = next((x for x in (install_time_dt, event_time_dt) if x is not None), None)
    if dt is None:
        logger.error(f"row {row}: times are not valid")
        return
    result = preprocess.common.construct_row_insert_data(row)
    if result is None:
        return
    if not (row[CommonColumns.event_name.name] == 'install'):
        logger.error(f"row {row}: {CommonColumns.event_name.name} is not valid")
        return
    user, campaign, event = result
    event[Event.event_time.name] = dt.to_pydatetime()
    event[Event.event_name.name] = row[CommonColumns.event_name.name]
    return user, campaign, event


# def check_row_validity(row: Series):
#     return construct_row_insert_data(row) is not None
#
#
# def insert_users(ser: Series):
#     values = list(ser)
#     with db_connector.conn.get_connection() as session:
#         with session.begin():
#             session.execute(psql.insert(User).values(values))


def read_file(file_path: str):
    try:
        df = pd.read_csv(file_path, header=0, index_col=0, parse_dates=[1, 2], na_filter=False)
    except (AssertionError, TypeError, ValueError) as exc:
        utils.logger.get_logger().error(f"error in processing {file_path}", exc_info=exc)
        return
    df.drop(columns=['event_revenue', 'event_revenue_usd'], inplace=True)

    t = time.time()
    print('validating installs data...')
    _data: DataFrame = df.apply(construct_row_insert_data, axis=1, result_type='expand')
    _data.columns = ['user_obj', 'campaign_obj', 'event_obj']
    df = df.join(_data)
    _cond = ((pd.notna(df['user_obj'])) & (pd.notna(df['campaign_obj'])) & (pd.notna(df['event_obj'])))
    df = df[_cond]
    print(f"validated in {time.time() - t} seconds")
    return df
