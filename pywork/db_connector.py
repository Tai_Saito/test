from typing import Optional

import sqlalchemy as sql
import sqlalchemy.orm as sql_orm
from sqlalchemy.orm import Session
from sqlalchemy.pool import NullPool, QueuePool

import cenv


class DBConnector:
    def __init__(self, is_main_process=False):
        connect_str = "postgresql+{driver}://{user}:{pw}@{host}:{port}/{db}".format(
            user=cenv.PGSQL_USER, pw=cenv.PGSQL_PASSWORD, host=cenv.PGSQL_HOST, port=cenv.PGSQL_PORT,
            db=cenv.PGSQL_DB, driver='psycopg2'
        )
        connect_args = {}
        if not is_main_process:
            connect_args.update({"prepared_statement_cache_size": 0, "statement_cache_size": 0})
        engine_options = {
            "connect_args": connect_args, "encoding": 'UTF8', "isolation_level": "READ COMMITTED",
            "pool_pre_ping": True,
            "poolclass": NullPool if is_main_process else QueuePool
        }
        if not is_main_process:
            engine_options.update({
                "pool_recycle": 3600, "pool_timeout": 10
            })
        self.engine = sql.create_engine(connect_str, **engine_options)
        self.session_factory = sql_orm.sessionmaker(bind=self.engine, autocommit=False, autoflush=True, expire_on_commit=False)
        self.scoped_session = sql_orm.scoped_session(self.session_factory)

    def get_connection(self) -> Session:
        return self.scoped_session()


conn: Optional[DBConnector] = None
