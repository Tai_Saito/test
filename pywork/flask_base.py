from flask import Flask, make_response, jsonify, request, render_template

import cenv

app = Flask(__name__, static_url_path='/static')
app.config['JSON_AS_ASCII'] = False


@app.route('/health')
def health():
    return "Working"


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')
