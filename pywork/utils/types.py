from enum import Enum


class CommonColumns(Enum):
    install_time = 1
    event_time = 2
    appsflyer_id = 3
    media_source = 4
    campaign = 5
    platform = 6
    event_name = 7
    event_revenue = 8
    event_revenue_usd = 9
