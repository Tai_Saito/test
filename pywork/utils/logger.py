import datetime
import inspect
import logging.handlers
import multiprocessing
import os.path
from logging import Formatter, Logger

import cenv


class CallStackFormatter(Formatter):
    """
    source: https://stackoverflow.com/questions/54747730/adding-stack-info-to-logging-format-in-python
    """

    @staticmethod
    def get_frame(index: int):
        # 0 = get_frame, index + 1 = index
        return inspect.stack()[index + 1]

    @staticmethod
    def get_caller_names(frame):
        if 'self' in frame.frame.f_locals:
            caller = type(frame.frame.f_locals['self']).__name__
        elif 'cls' in frame.frame.f_locals:
            caller = frame.frame.f_locals['cls'].__name__
        else:
            caller = os.path.split(frame.filename)[-1].split('.')
            # caller = os.path.split(frame.filename)[0].split('.')
            caller = '.'.join(caller[:-1])
        return f"{caller}.{frame.function}"

    def formatStack(self, _=None) -> str:
        # 0 = formatStack, 11 = func
        frame = self.get_frame(11)
        return self.get_caller_names(frame)

    def format(self, record):
        record.message = record.getMessage()
        record.stack_info = self.formatStack()
        if self.usesTime():
            record.asctime = self.formatTime(record, self.datefmt)
        s = self.formatMessage(record)
        if record.exc_info:
            # Cache the traceback text to avoid converting it multiple times
            # (it's constant anyway)
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + record.exc_text
        return s


def setup_logger(name: str, level=logging.WARNING, filename: str = None, mode: str = 'w',
                 launch_rotating: bool = False, day_rotating: bool = False, propagate=False) -> logging.Logger:
    formatter = CallStackFormatter('[%(asctime)s.%(msecs)03d] [%(levelname)-5s] '
                                   '[%(stack_info)+37s ] %(message)s',
                                   '%d.%m.%Y %H:%M:%S')
    logger = logging.getLogger(name)
    logger.propagate = propagate
    logger.setLevel(level)

    logger.handlers = []

    if filename:
        if launch_rotating:
            filename = f"{filename}_{datetime.datetime.now().strftime('%m.%d_%H:%M:%S')}"
        filepath = os.path.join(cenv.PATH_LOGS, f"{filename}.log")
        if day_rotating:
            handler = logging.handlers.TimedRotatingFileHandler(filepath, when='midnight')
        elif not launch_rotating:
            handler = logging.handlers.RotatingFileHandler(filepath, mode, backupCount=5)
            handler.doRollover()
        else:
            handler = logging.FileHandler(filepath, mode)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    return logger


def get_logger() -> Logger:
    """ Правила уровней логирования:
    - debug: проверка измененных данных
    - info: когда алгоритм переходит от одной стадии к другой
    - warning: когда случаются некритичные, но непредусмотренные вещи
    """
    name = multiprocessing.current_process().name
    if name == 'MainProcess':
        name = 'root'
    return logging.getLogger(name)
