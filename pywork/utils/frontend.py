from typing import Dict, Any, Optional


def init_error_codes(codes: Dict[int, str]):
    """ Создание функции для возвращения указанных кодов ошибок в jsonify-виде

    Аргументы:
        - codes: словарь с кодами ошибок

    Возвращаемое значение:
        - callable: функция для возвращения ошибки по ее коду
    """
    def error_code(code: int = 10, msg='') -> Dict[str, Any]:
        if len(msg):
            msg = f" ({msg})"
        return {
            "error": True,
            "error_code": code,
            "message": f"{codes[code]}{msg}"
        }
    return error_code


def create_response(data: Optional[Any] = None, message='OK'):
    d = {
        "error": False,
        "error_code": None,
        "message": message
    }
    if data is not None:
        d['data'] = data
    return d
