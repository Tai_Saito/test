from datetime import datetime, timedelta
from typing import Optional

import sqlalchemy as sql
import sqlalchemy.future as sql_future
from flask import request
from sqlalchemy.future import Select

import db_connector
import flask_base
import utils.frontend
import utils.logger
from db_connector import DBConnector
from tables.campaigns import Campaign
from tables.events import Event
from tables.purchases import Purchase
from tables.users import User


def internal_make_report(conn: DBConnector, start_date: datetime, end_date: datetime, platform: str, media_source: str,
                         campaign: Optional[str]):
    end_date = end_date + timedelta(days=1)

    query_installs: Select = sql_future.select(
        Campaign.name,
        sql.func.count(Event.id).label('insts')
    )
    query_installs = query_installs.join_from(Campaign, Event, Campaign.name == Event.campaign_name, isouter=True)
    query_installs = query_installs.join(User, Event.user_id == User.appsflyer_id)
    query_installs = query_installs.filter(
        Event.event_name == 'install', start_date <= Event.event_time, Event.event_time < end_date,
        User.platform == platform, Campaign.media_source == media_source
    )
    query_installs = query_installs.group_by(Campaign.name)
    query_installs_subq = query_installs.subquery()

    query_events: Select = sql_future.select(
        Campaign.name,
        sql.func.sum(Purchase.event_revenue).label('rev'),
        sql.func.sum(Purchase.event_revenue_usd).label('rev_usd')
    )
    query_events = query_events.join_from(Campaign, Event, Campaign.name == Event.campaign_name, isouter=True)
    query_events = query_events.join(User, Event.user_id == User.appsflyer_id)
    query_events = query_events.join(Purchase, Purchase.event_id == Event.id)
    query_events = query_events.filter(
        start_date <= Event.event_time, Event.event_time < end_date,
        User.platform == platform, Campaign.media_source == media_source
    )
    query_events = query_events.group_by(Campaign.name)
    query_events_subq = query_events.subquery()

    col_installs = sql.func.coalesce(query_installs_subq.c['insts'], 0)
    col_revenue = sql.func.coalesce(query_events_subq.c['rev'], 0)
    col_revenue_usd = sql.func.coalesce(query_events_subq.c['rev_usd'], 0)
    query: sql_future.Select = sql_future.select(
        Campaign.name.label('campaign'),
        col_installs.label('installs'),
        col_revenue.label('revenue'),
        col_revenue_usd.label('revenue_usd')
    )
    query = query.join_from(Campaign, query_installs_subq, Campaign.name == query_installs_subq.c['name'], isouter=True)
    query = query.join(query_events_subq, Campaign.name == query_events_subq.c['name'], isouter=True)
    query = query.filter(sql.or_(
        col_installs > 0, col_revenue > 0, col_revenue_usd > 0
    ))
    if campaign is not None:
        query = query.filter(Campaign.name == campaign)

    with conn.get_connection() as session:
        campaigns = session.execute(query).all()
        return campaigns


error_code = utils.frontend.init_error_codes({
    9: "плохой запрос",
    10: "неизвестная ошибка",
})


@flask_base.app.route('/api/make_report', methods=['GET'], strict_slashes=False)
def make_report():
    logger = utils.logger.get_logger()
    logger.debug(f"handling request...")
    for k in ('start_date', 'end_date', 'platform', 'media_source'):
        if k not in request.args:
            return error_code(9, f"отсутствует параметр {k}")
        if len(request.args[k]) == 0:
            return error_code(9, f"параметр {k} обязателен")
    try:
        start_date = datetime.fromisoformat(request.args['start_date'])
    except ValueError:
        return error_code(9, f"параметр start_date невалиден")
    try:
        end_date = datetime.fromisoformat(request.args['end_date'])
    except ValueError:
        return error_code(9, f"параметр end_date невалиден")
    if not (start_date <= end_date):
        return error_code(9, f"start_date должен быть раньше end_date")
    platform = request.args['platform']
    media_source = request.args['media_source']
    campaign = request.args.get('campaign', None)
    data = internal_make_report(db_connector.conn, start_date, end_date, platform, media_source, campaign)
    for i, row in enumerate(data):
        data[i] = dict(row)
    return utils.frontend.create_response(data)
