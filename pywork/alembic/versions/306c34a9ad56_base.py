"""base

Revision ID: 306c34a9ad56
Revises: 
Create Date: 2022-08-23 23:10:39.146511

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '306c34a9ad56'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('campaigns',
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('media_source', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('name', name='campaigns_name_pk'),
    sa.UniqueConstraint('name', name='campaigns_name_unique')
    )
    op.create_table('users',
    sa.Column('appsflyer_id', sa.String(), nullable=False),
    sa.Column('platform', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('appsflyer_id', name='users_appsflyer_id_pk'),
    sa.UniqueConstraint('appsflyer_id', name='users_appsflyer_id_unique')
    )
    op.create_table('events',
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.Column('event_time', sa.DateTime(), nullable=False),
    sa.Column('event_name', sa.String(), nullable=False),
    sa.Column('user_id', sa.String(), nullable=True),
    sa.Column('campaign_name', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['campaign_name'], ['campaigns.name'], name='events_campaign_name_fk', onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['user_id'], ['users.appsflyer_id'], name='events_user_id_fk', onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id', name='events_id_pk'),
    sa.UniqueConstraint('id', name='events_id_unique')
    )
    op.create_table('purchases',
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.Column('event_revenue', sa.Float(), nullable=False),
    sa.Column('event_revenue_usd', sa.Float(), nullable=False),
    sa.Column('event_id', sa.BIGINT(), nullable=True),
    sa.ForeignKeyConstraint(['event_id'], ['events.id'], name='purchases_event_id_fk', onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id', name='purchases_id_pk'),
    sa.UniqueConstraint('id', name='purchases_id_unique')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('purchases')
    op.drop_table('events')
    op.drop_table('users')
    op.drop_table('campaigns')
    # ### end Alembic commands ###
