import os.path

import sqlalchemy.orm


DEBUG = False

BASE = sqlalchemy.orm.declarative_base()

if DEBUG:
    import dotenv
    dotenv.load_dotenv(os.path.join(os.pardir, '.env'))

PGSQL_USER = os.getenv('PGSQL_USER')
PGSQL_PASSWORD = os.getenv('PGSQL_PASSWORD')
PGSQL_DB = os.getenv('PGSQL_DB')

DB_SERVERNAME = os.getenv('DB_SERVERNAME') if not DEBUG else 'LOCAL'
PGSQL_HOST = os.getenv(f"PGSQL_{DB_SERVERNAME}_HOST")
PGSQL_PORT = os.getenv(f"PGSQL_{DB_SERVERNAME}_PORT_INT")

FLASK_PORT = os.getenv(f"PYWORK_PORT_INT")

PATH_PYDIR = os.path.abspath(os.path.dirname(__file__))

DIR_LOGS = 'runtime'
PATH_LOGS = os.path.join(PATH_PYDIR, DIR_LOGS)

for _1 in (PATH_LOGS, ):
    os.makedirs(_1, exist_ok=True)
