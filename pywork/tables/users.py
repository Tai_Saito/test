import time

import pandas as pd
import sqlalchemy.dialects.postgresql as psql
from pandas import DataFrame
from sqlalchemy import Column, String, PrimaryKeyConstraint, UniqueConstraint

import cenv
import db_connector
from utils.types import CommonColumns


class User(cenv.BASE):
    __tablename__ = 'users'

    appsflyer_id = Column(CommonColumns.appsflyer_id.name, String, nullable=False)
    platform = Column(CommonColumns.platform.name, String, nullable=False)

    __table_args__ = (
        PrimaryKeyConstraint(appsflyer_id.name, name=f"{__tablename__}_{appsflyer_id.name}_pk"),
        UniqueConstraint(appsflyer_id.name, name=f"{__tablename__}_{appsflyer_id.name}_unique")
    )


def insert_from_dataframes(*dfs: DataFrame):
    t = time.time()
    print('inserting users...')
    cols = [CommonColumns.appsflyer_id.name, 'user_obj']
    data = pd.DataFrame(columns=cols)
    for df in dfs:
        df = df[cols]
        data = pd.concat([data, df], ignore_index=True)
    data = data.drop_duplicates(subset=cols[:-1])
    values = list(data[cols[-1]])
    with db_connector.conn.get_connection() as session:
        with session.begin():
            session.execute(psql.insert(User).values(values))
    print(f"inserted users in {time.time() - t}")
