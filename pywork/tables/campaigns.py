import time

import pandas as pd
import sqlalchemy.dialects.postgresql as psql
from pandas import DataFrame
from sqlalchemy import Column, String, PrimaryKeyConstraint, UniqueConstraint

import cenv
import db_connector
from utils.types import CommonColumns


class Campaign(cenv.BASE):
    __tablename__ = 'campaigns'

    name = Column('name', String, nullable=False)
    media_source = Column(CommonColumns.media_source.name, String, nullable=False)

    __table_args__ = (
        PrimaryKeyConstraint(name.name, name=f"{__tablename__}_{name.name}_pk"),
        UniqueConstraint(name.name, name=f"{__tablename__}_{name.name}_unique")
    )


def insert_from_dataframes(*dfs: DataFrame):
    t = time.time()
    print('inserting campaigns...')
    cols = [CommonColumns.campaign.name, 'campaign_obj']
    data = pd.DataFrame(columns=cols)
    for df in dfs:
        df = df[cols]
        data = pd.concat([data, df], ignore_index=True)
    data = data.drop_duplicates(subset=cols[:-1])
    values = list(data[cols[-1]])
    with db_connector.conn.get_connection() as session:
        with session.begin():
            session.execute(psql.insert(Campaign).values(values))
    print(f"inserted campaigns in {time.time() - t}")
