import time

import sqlalchemy.dialects.postgresql as psql
import sqlalchemy.orm as sql_orm
from pandas import DataFrame, Series
from sqlalchemy import Column, BIGINT, Float, ForeignKey, PrimaryKeyConstraint, UniqueConstraint

import cenv
import db_connector
from tables.events import Event
from utils.types import CommonColumns


class Purchase(cenv.BASE):
    __tablename__ = 'purchases'

    id = Column('id', BIGINT, autoincrement=True)
    event_revenue = Column(CommonColumns.event_revenue.name, Float, nullable=False)
    event_revenue_usd = Column(CommonColumns.event_revenue_usd.name, Float, nullable=False)
    event_id = Column('event_id', ForeignKey(
        f"{Event.__tablename__}.{Event.id.name}", name=f"{__tablename__}_event_id_fk",
        onupdate="CASCADE", ondelete="CASCADE"
    ))

    event = sql_orm.relationship(Event, innerjoin=True, lazy="selectin", cascade_backrefs=False)

    __table_args__ = (
        PrimaryKeyConstraint(id.name, name=f"{__tablename__}_{id.name}_pk"),
        UniqueConstraint(id.name, name=f"{__tablename__}_{id.name}_unique")
    )


def update_purchase_event_id(row: Series):
    row['purchase_obj'][Purchase.event_id] = row['event_id']
    return row['purchase_obj']


def insert_from_dataframe(df: DataFrame):
    t = time.time()
    print('inserting purchases...')
    df = df[df[CommonColumns.event_name.name] == 'af_purchase']
    values = list(df.apply(update_purchase_event_id, axis=1))
    with db_connector.conn.get_connection() as session:
        with session.begin():
            session.execute(psql.insert(Purchase).values(values))
    print(f"inserted purchases in {time.time() - t}")
