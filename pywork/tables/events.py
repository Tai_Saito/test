import time

import pandas as pd
import sqlalchemy.dialects.postgresql as psql
import sqlalchemy.orm as sql_orm
from pandas import DataFrame, Series
from sqlalchemy import Column, BIGINT, DateTime, String, ForeignKey, PrimaryKeyConstraint, UniqueConstraint

import cenv
import db_connector
from tables.campaigns import Campaign
from tables.users import User
from utils.types import CommonColumns


class Event(cenv.BASE):
    __tablename__ = 'events'

    id = Column('id', BIGINT, autoincrement=True)
    event_time = Column(CommonColumns.event_time.name, DateTime, nullable=False)
    event_name = Column(CommonColumns.event_name.name, String, nullable=False)
    user_id = Column('user_id', ForeignKey(
        f"{User.__tablename__}.{User.appsflyer_id.name}", name=f"{__tablename__}_user_id_fk",
        onupdate="CASCADE", ondelete="CASCADE"
    ))
    campaign_name = Column('campaign_name', ForeignKey(
        f"{Campaign.__tablename__}.{Campaign.name.name}", name=f"{__tablename__}_campaign_name_fk",
        onupdate="CASCADE", ondelete="CASCADE"
    ))

    user = sql_orm.relationship(User, innerjoin=True, lazy="selectin", cascade_backrefs=False)
    campaign = sql_orm.relationship(Campaign, innerjoin=True, lazy="selectin", cascade_backrefs=False)

    __table_args__ = (
        PrimaryKeyConstraint(id.name, name=f"{__tablename__}_{id.name}_pk"),
        UniqueConstraint(id.name, name=f"{__tablename__}_{id.name}_unique")
    )


def insert_from_dataframes(*dfs: DataFrame):
    t = time.time()
    print('inserting events...')
    for df in dfs:
        values = list(df['event_obj'])
        with db_connector.conn.get_connection() as session:
            with session.begin():
                events = session.execute(psql.insert(Event).values(values).returning(Event.id)).scalars().all()
        df['event_id'] = events
    print(f"inserted events in {time.time() - t}")
